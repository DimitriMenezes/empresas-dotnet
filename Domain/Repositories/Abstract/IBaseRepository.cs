﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;

namespace Domain.Repositories.Abstract
{
    public interface IBaseRepository<TEntity>
    {
        TEntity GetById(int id);
        IQueryable<TEntity> GetAll();
        IQueryable<TEntity> GetAll(string[] includes);
        void Insert(TEntity entity);
        void Update(TEntity entity);
        void Delete(int id);        
    }
}
