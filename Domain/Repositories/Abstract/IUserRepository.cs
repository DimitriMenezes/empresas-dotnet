﻿using Domain.Entities;
using Domain.Repositories.Concrete;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Repositories.Abstract
{
    public interface IUserRepository : IBaseRepository<UserLogin>
    {

    }
}
