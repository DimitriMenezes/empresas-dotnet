﻿using Domain.Entities;
using Domain.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Repositories.Concrete
{
    public class EnterpriseRepository : BaseRepository<Enterprise>, IEnterpriseRepository
    {

    }
}
