﻿using Domain.Entities;
using Domain.Repositories.Abstract;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Repositories.Concrete
{
    public class InvestorRepository : BaseRepository<Investor>, IInvestorRepository
    {

    }
}
