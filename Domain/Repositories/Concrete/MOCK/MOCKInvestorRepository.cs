﻿using Domain.Entities;
using Domain.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Repositories.Concrete.MOCK
{
    public class MOCKInvestorRepository : IInvestorRepository
    {
        private List<Investor> listInvestors { get; set; }

        public MOCKInvestorRepository()
        {
            listInvestors = new List<Investor>
            {
                new Investor
                {
                    Id = 1,
                    Balance = 1111,
                    FirstAccess = false,
                    Email = "dimitri.menezes@gmail.com",
                    IsSuperAngel = false,
                    Name = "Dimitri Enterprise",
                    Photo = null,
                    AddressId = 1,
                    Address = new Address
                    {
                        Id = 1,
                        City = "Aracaju-Se",
                        Country = "Brasil"
                    }
                },
                new Investor
                {
                    Id = 2,
                    Balance = 222,
                    FirstAccess = false,
                    Email = "teste@gmail.com",
                    IsSuperAngel = false,
                    Name = "Teste Enterprise",
                    Photo = null,
                    AddressId = 2,
                    Address = new Address
                    {
                        Id = 2,
                        City = "Lagarto-Se",
                        Country = "Brasil"
                    }
                },
                new Investor
                {
                    Id = 3,
                    Balance = 23333,
                    FirstAccess = false,
                    Email = "investor@gmail.com",
                    IsSuperAngel = false,
                    Name = "Investor Enterprise",
                    Photo = null,
                    AddressId = 2,
                    Address = new Address
                    {
                        Id = 3,
                        City = "Sao Paulo-SP",
                        Country = "Brasil"
                    }
                },
            };
        }

        public void Delete(int id)
        {
            var investor = listInvestors.Find(i => i.Id == id);
            listInvestors.Remove(investor);
        }

        public IQueryable<Investor> GetAll()
        {
            return listInvestors.AsQueryable();
        }

        public IQueryable<Investor> GetAll(string[] includes)
        {
            return listInvestors.AsQueryable();
        }

        public Investor GetById(int id)
        {
            return listInvestors.Find(i=> i.Id == id);
        }

        public void Insert(Investor entity)
        {
            listInvestors.Add(entity);
        }

        public void Update(Investor entity)
        {
            
        }
    }
}
