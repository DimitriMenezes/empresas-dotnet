﻿using Domain.Entities;
using Domain.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Repositories.Concrete.MOCK
{
    public class MOCKUserRepository : IUserRepository
    {
        private List<UserLogin> _usersList { get; set; }
        public MOCKUserRepository()
        {
            _usersList = new List<UserLogin>()
            {
                new UserLogin
                {
                    Id = 1,
                    Email = "dimitri.menezes@gmail.com",
                    Password = "6ca13d52ca70c883e0f0bb101e425a89e8624de51db2d2392593af6a84118090"
                },
                new UserLogin
                {
                    Id = 2,
                    Email = "teste@gmail.com",
                    Password = "6ca13d52ca70c883e0f0bb101e425a89e8624de51db2d2392593af6a84118090"
                },
                new UserLogin
                {
                    Id = 3,
                    Email = "investor@gmail.com",
                    Password = "6ca13d52ca70c883e0f0bb101e425a89e8624de51db2d2392593af6a84118090"
                }
            };

        }
            
        public void Delete(int id)
        {
            var user = _usersList.Find(i => i.Id == id);
            _usersList.Remove(user);
        }

        public IQueryable<UserLogin> GetAll()
        {
            return _usersList.AsQueryable();
        }

        public IQueryable<UserLogin> GetAll(string[] includes)
        {
            return _usersList.AsQueryable();
        }

        public UserLogin GetById(int id)
        {
            return _usersList.Find(i => i.Id == id);
        }

        public void Insert(UserLogin entity)
        {
            _usersList.Add(entity);
        }

        public void Update(UserLogin entity)
        {
            
        }
    }
}
