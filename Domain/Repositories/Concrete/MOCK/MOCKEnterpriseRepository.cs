﻿using Domain.Entities;
using Domain.Repositories.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Domain.Repositories.Concrete.MOCK
{
    public class MOCKEnterpriseRepository : IEnterpriseRepository
    {
        private List<Enterprise> _enterpriseList { get; set; }
        public MOCKEnterpriseRepository()
        {
            _enterpriseList = new List<Enterprise>
            {
                new Enterprise
                {
                    Id = 1,
                    Facebook = null,
                    Linkedin = null,
                    Phone = null,
                    Photo = null,
                    OwnShares = 1,
                    OwnEnterprise = false,                    
                    Shares = 99,
                    Twitter = null,
                    Value = 50,
                    SharePrice = 1000,
                    Name = "Enterprise Fake",
                    Email = "enterprise@gmail.com",                   
                    Description = "Lorem ipsum dolor sit amet, " +
                    "consectetur adipiscing elit, sed do eiusmod tempor " +
                    "incididunt ut labore et dolore magna aliqua. " +
                    "Ut enim ad minim veniam, quis nostrud exercitation " +
                    "ullamco laboris nisi ut aliquip ex ea commodo consequat. " +
                    "Duis aute irure dolor in reprehenderit " +
                    "in voluptate velit esse cillum dolore eu fugiat nulla pariatur",                    
                    EnterpriseTypeId = 1,
                    EnterpriseType = new EnterpriseType
                    {
                        Id = 1,
                        Name = "Software"                    
                    },
                    Address =  new Address{
                        Id = 1,
                        City = "Aracaju-Se",
                        Country = "Brasil"
                    },
                    AddressId = 1
                },
                   new Enterprise
                {
                    Id = 2,
                    Facebook = null,
                    Linkedin = null,
                    Phone = "",
                    Photo = "",
                    OwnShares = 1,
                    OwnEnterprise = false,
                    Shares = 99,
                    Twitter = null,
                    Value = 510,
                    SharePrice = 10020,
                    Name = "Enterprise Fake",
                    Email = "enterprise@gmail.com",
                    Description = "Labore et dolore magna aliqua. " +
                    "Ut enim ad minim veniam, quis nostrud exercitation " +
                    "ullamco laboris nisi ut aliquip ex ea commodo consequat. " +
                    "Duis aute irure dolor in reprehenderit " +
                    "in voluptate velit esse cillum dolore eu fugiat nulla pariatur",
                    EnterpriseTypeId = 1,
                    EnterpriseType = new EnterpriseType
                    {
                        Id = 2,
                        Name = "Health"
                    },
                    Address =  new Address{
                        Id = 1,
                        City = "Aracaju-Se",
                        Country = "Brasil"
                    },
                    AddressId = 1
                }
            };
        }
        public void Delete(int id)
        {
            var enterprise = _enterpriseList.Find(i => i.Id == id);
            _enterpriseList.Remove(enterprise);
        }

        public IQueryable<Enterprise> GetAll()
        {
            return _enterpriseList.AsQueryable();
        }

        public IQueryable<Enterprise> GetAll(string[] includes)
        {
            return _enterpriseList.AsQueryable();
        }

        public Enterprise GetById(int id)
        {
            return _enterpriseList.Find(i => i.Id == id);
        }

        public void Insert(Enterprise entity)
        {
            
        }

        public void Update(Enterprise entity)
        {
           
        }
    }
}
