﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class EnterpriseInvestor
    {
        public int InvestorId { get; set; }
        public int EnterpriseId { get; set; }

        public virtual Enterprise Enterprise { get; set; }
        public virtual Investor Investor { get; set; }
    }
}
