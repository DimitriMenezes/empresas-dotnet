﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class Investor
    {
        public Investor()
        {
            EnterpriseInvestor = new HashSet<EnterpriseInvestor>();
            EnterpriseOwner = new HashSet<EnterpriseOwner>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public decimal Balance { get; set; }
        public string Photo { get; set; }
        public bool FirstAccess { get; set; }
        public bool IsSuperAngel { get; set; }
        public int AddressId { get; set; }

        public virtual Address Address { get; set; }
        public virtual ICollection<EnterpriseInvestor> EnterpriseInvestor { get; set; }
        public virtual ICollection<EnterpriseOwner> EnterpriseOwner { get; set; }
    }
}
