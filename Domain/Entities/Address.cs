﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class Address
    {
        public Address()
        {
            Enterprise = new HashSet<Enterprise>();
            Investor = new HashSet<Investor>();
        }

        public int Id { get; set; }
        public string City { get; set; }
        public string Country { get; set; }

        public virtual ICollection<Enterprise> Enterprise { get; set; }
        public virtual ICollection<Investor> Investor { get; set; }
    }
}
