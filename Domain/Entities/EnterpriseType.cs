﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class EnterpriseType
    {
        public EnterpriseType()
        {
            Enterprise = new HashSet<Enterprise>();
        }

        public int Id { get; set; }
        public string Name { get; set; }

        public virtual ICollection<Enterprise> Enterprise { get; set; }
    }
}
