﻿using System;
using System.Collections.Generic;

namespace Domain.Entities
{
    public partial class Enterprise
    {
        public Enterprise()
        {
            EnterpriseInvestor = new HashSet<EnterpriseInvestor>();
            EnterpriseOwner = new HashSet<EnterpriseOwner>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string Facebook { get; set; }
        public string Twitter { get; set; }
        public string Linkedin { get; set; }
        public string Phone { get; set; }
        public bool OwnEnterprise { get; set; }
        public string Photo { get; set; }
        public int Value { get; set; }
        public int Shares { get; set; }
        public decimal SharePrice { get; set; }
        public int OwnShares { get; set; }
        public int EnterpriseTypeId { get; set; }
        public int AddressId { get; set; }
        public string Description { get; set; }

        public virtual Address Address { get; set; }
        public virtual EnterpriseType EnterpriseType { get; set; }
        public virtual ICollection<EnterpriseInvestor> EnterpriseInvestor { get; set; }
        public virtual ICollection<EnterpriseOwner> EnterpriseOwner { get; set; }
    }
}
