CREATE TABLE UserLogin
(	Id int IDENTITY(1,1) NOT NULL,
	Email varchar(255) NOT NULL,
	Password varchar(255) NOT NULL,
	CONSTRAINT PK_UserLogin PRIMARY KEY(Id)	
);

CREATE TABLE Address(
	Id int IDENTITY(1,1) NOT NULL,
	City varchar(255) NULL,
	Country varchar(255) NULL,
	CONSTRAINT PK_Address PRIMARY KEY(Id)
);

CREATE TABLE Enterprise(
	Id int IDENTITY(1,1) NOT NULL,
	Name varchar(255) NOT NULL,
	Email varchar(255) NOT NULL,
	Facebook varchar(255) NULL,
	Twitter varchar(255) NULL,
	Linkedin varchar(255) NULL,
	Phone varchar(255) NULL,
	OwnEnterprise bit NOT NULL,
	Photo varchar(255) NULL,
	Value int NOT NULL,
	Shares int NOT NULL,
	SharePrice decimal(18, 2) NOT NULL,
	OwnShares int NOT NULL,
	EnterpriseTypeId int NOT NULL,
	AddressId int NOT NULL,
	Description varchar(8000) NOT NULL,
	CONSTRAINT PK_Enterprise PRIMARY KEY(Id)
);

CREATE TABLE EnterpriseType (
	Id int IDENTITY(1,1) NOT NULL,
	Name varchar(255) NOT NULL,
	CONSTRAINT PK_EnterpriseType PRIMARY KEY(Id) 
);

CREATE TABLE Investor(
	Id int IDENTITY(1,1) NOT NULL,
	Name varchar(255) NOT NULL,
	Email varchar(255) NOT NULL,
	Balance decimal(18, 2) NOT NULL,
	Photo varchar(255) NULL,
	FirstAccess bit NOT NULL,
	IsSuperAngel bit NOT NULL,
	AddressId int NOT NULL,
	CONSTRAINT PK_Investor PRIMARY KEY(Id) 
);

CREATE TABLE EnterpriseInvestor(
	InvestorId int NOT NULL,
	EnterpriseId int NOT NULL,
	CONSTRAINT PK_EnterpriseInvestor PRIMARY KEY(InvestorId,EnterpriseId) 
);

CREATE TABLE EnterpriseOwner(
	InvestorId int NOT NULL,
	EnterpriseId int NOT NULL,
	CONSTRAINT PK_EnterpriseOwner PRIMARY KEY(InvestorId,EnterpriseId) 
);

ALTER TABLE Enterprise ADD CONSTRAINT FK_Enterprise_Address 
FOREIGN KEY(AddressId) REFERENCES Address (Id)

ALTER TABLE Enterprise ADD CONSTRAINT FK_Enterprise_EnterpriseType 
FOREIGN KEY(EnterpriseTypeId) REFERENCES EnterpriseType (Id)

ALTER TABLE Investor ADD CONSTRAINT FK_Investor_Address 
FOREIGN KEY(AddressId) REFERENCES Address (Id)

ALTER TABLE EnterpriseInvestor ADD CONSTRAINT FK_EnterpriseInvestor_Investor
FOREIGN KEY (InvestorId) REFERENCES Investor(Id);

ALTER TABLE EnterpriseInvestor
ADD CONSTRAINT FK_EnterpriseInvestor_Enterprise
FOREIGN KEY (EnterpriseId) REFERENCES Enterprise(Id);

ALTER TABLE EnterpriseOwner
ADD CONSTRAINT FK_EnterpriseOwner_Investor
FOREIGN KEY (InvestorId) REFERENCES Investor(Id);

ALTER TABLE EnterpriseOwner
ADD CONSTRAINT FK_EnterpriseOwner_Enterprise
FOREIGN KEY (EnterpriseId) REFERENCES Enterprise(Id);