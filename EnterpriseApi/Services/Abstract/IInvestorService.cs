﻿using EnterpriseApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseApi.Services.Abstract
{
    public interface IInvestorService
    {
        public DTOInvestor GetInvestorByEmail(string email);
    }
}
