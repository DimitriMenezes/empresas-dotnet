﻿using EnterpriseApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseApi.Services.Abstract
{
    public interface IEnterpriseService
    {
        public DTOEnterprise GetEnterpriseById(int id);
        public List<DTOEnterprise> GetEnterprises();
        public List<DTOEnterprise> GetEnterprises(int enterprise_types, string name);
    }
}
