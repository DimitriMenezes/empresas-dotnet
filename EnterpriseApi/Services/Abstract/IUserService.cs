﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseApi.Services.Abstract
{
    public interface IUserService
    {
        public bool IsAuthenticated(string email, string password);
    }
}
