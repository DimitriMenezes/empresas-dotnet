﻿using Domain.Entities;
using Domain.Repositories.Abstract;
using EnterpriseApi.Models;
using EnterpriseApi.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseApi.Services.Concrete
{
    public class PortfolioService : IPortfolioService
    {
        private readonly IInvestorRepository _investorRepository;
        private readonly IEnterpriseService _enterpriseService;

        public PortfolioService(IInvestorRepository investorRepository, 
            IEnterpriseService enterpriseService)
        {
            _investorRepository = investorRepository;
            _enterpriseService = enterpriseService;
        }

        public DTOPortfolio CreateInvestorPortfolio(int investorId)
        {
            string[] includes = { "EnterpriseInvestor",
                "EnterpriseInvestor.Enterprise", "EnterpriseInvestor.Enterprise.EnterpriseType" };
            var investor = _investorRepository.GetAll(includes).Where(i => i.Id == investorId)
                .FirstOrDefault();

            if (investor == null)
                return null;
         
            var enterpriseInvestor = investor.EnterpriseInvestor;
            if (!enterpriseInvestor.Any())
                return null;

            var listEnterprises = new List<DTOEnterprise>();
            var dto = new DTOPortfolio
            {
                enterprises_number = enterpriseInvestor.Count(),
                enterprises = listEnterprises
            };
            foreach (var enterprise in enterpriseInvestor)
            {
                listEnterprises.Add(_enterpriseService.GetEnterpriseById(enterprise.EnterpriseId));
            }
            return dto;
        }      
        
        public decimal CalculatePortfolioValue(int investorId)
        {
            // Não faço ideia de como implementar a Regra de negocio
            return 0;
        }    
    }
}
