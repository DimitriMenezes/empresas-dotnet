﻿using AutoMapper;
using Domain.Entities;
using Domain.Repositories.Abstract;
using EnterpriseApi.Models;
using EnterpriseApi.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseApi.Services.Concrete
{
    public class InvestorService : IInvestorService
    {
        private readonly IInvestorRepository _investorRepository;
        private readonly IMapper _mapper;

        public InvestorService(IInvestorRepository investorRepository, IMapper mapper)
        {
            _investorRepository = investorRepository;
            _mapper = mapper;
        }

        public DTOInvestor GetInvestorByEmail(string email)
        {
            string[] includes = { "Address" };
            var investor = _investorRepository.GetAll(includes).Where(i => i.Email == email)
                .FirstOrDefault();

            if (investor == null)
                throw new Exception(); 

            return DTOMapper(investor);
        }
        
        private DTOInvestor DTOMapper(Investor investor)
        {
            return _mapper.Map<Investor, DTOInvestor>(investor);
        }     
    }
}
