﻿using AutoMapper;
using Domain.Entities;
using Domain.Repositories.Abstract;
using EnterpriseApi.Models;
using EnterpriseApi.Services.Abstract;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseApi.Services.Concrete
{
    public class EnterpriseService : IEnterpriseService
    {
        private readonly IEnterpriseRepository _enterpriseRepository;
        private readonly IMapper _mapper;

        public EnterpriseService(IEnterpriseRepository enterpriseRepository, IMapper mapper)
        {
            _enterpriseRepository = enterpriseRepository;
            _mapper = mapper;
        }

        public DTOEnterprise GetEnterpriseById(int id)
        {
            if (id <= 0)
                return null;

            var enterprise = EnterprisesQuery()
                .Where(i => i.Id == id).FirstOrDefault();

            if(enterprise != null)
            {
                var dto = DTOMapper(enterprise);                
                return dto;
            }
            return null;            
        }
        public List<DTOEnterprise> GetEnterprises()
        {
            var query = EnterprisesQuery();        

            var enterpriseModelLists = new List<DTOEnterprise>();

            foreach (var enterprise in query)
            {
                var dto = DTOMapper(enterprise);
                enterpriseModelLists.Add(dto);
            }
            return enterpriseModelLists;
        }

        public List<DTOEnterprise> GetEnterprises(int enterprise_types, string name)
        {
            var query = EnterprisesQuery();

            query = query.Where(i => i.EnterpriseTypeId == enterprise_types);

            if(name != null)
                query = query.Where(i => i.Name.Contains(name));

            var enterpriseModelLists = new List<DTOEnterprise>();

            foreach (var enterprise in query)
            {
                var dto = DTOMapper(enterprise);
                enterpriseModelLists.Add(dto);
            }
            return enterpriseModelLists;
        }

        private IQueryable<Enterprise> EnterprisesQuery()
        {
            string[] includes = { "Address", "EnterpriseType" };
            return _enterpriseRepository.GetAll(includes);
        }

        private DTOEnterprise DTOMapper(Enterprise enterprise)
        {
           return _mapper.Map<Enterprise, DTOEnterprise>(enterprise);         
        }
    }
}
