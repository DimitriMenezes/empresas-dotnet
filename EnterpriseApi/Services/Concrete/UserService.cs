﻿using Domain.Repositories.Abstract;
using EnterpriseApi.Models;
using EnterpriseApi.Services.Abstract;
using EnterpriseApi.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseApi.Services.Concrete
{
    public class UserService : IUserService
    {
        private readonly IUserRepository _userRepository;

        public UserService(IUserRepository userRepository)
        {
            _userRepository = userRepository;
        }

        public bool IsAuthenticated(string email, string password)
        {
            if ((email == "" && email == null) ||
               (password == "" && password == null))
            {
                return false;
            }          

            var user = _userRepository.GetAll().Where(i => i.Email == email);
            if (!user.Any())
                return false;

            var passwordEncrypted = Criptografia.GeneratePasswordSha256(password);

            if (user.FirstOrDefault().Password.ToUpper() == passwordEncrypted.ToUpper())
                return true;
            else
                return false;
        }
    }
}
