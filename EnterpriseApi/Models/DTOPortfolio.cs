﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseApi.Models
{
    public class DTOPortfolio
    {
        public int enterprises_number { get; set; }
        public List<DTOEnterprise> enterprises { get; set; }
    }
}
