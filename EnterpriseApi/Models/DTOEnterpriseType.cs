﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseApi.Models
{
    public class DTOEnterpriseType
    {
        public int id { get; set; }
        public string enterprise_type_name { get; set; }
    }
}
