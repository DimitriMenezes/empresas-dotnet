﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace EnterpriseApi.Models
{
    public class DTOUserAuthentication
    {
        public string email { get; set; }
        public  string password { get; set; }
    }
}
