using Domain.Context;
using Domain.Repositories.Abstract;
using Domain.Repositories.Concrete;
using EnterpriseApi.Services.Concrete;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.AspNetCore.Authentication.Cookies;
using Microsoft.Extensions.Logging;
using EnterpriseApi.Services.Abstract;
using AutoMapper;

namespace EnterpriseApi
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddAutoMapper(typeof(Startup));

            services.AddDbContext<ModelContext>(options =>
                options.UseSqlServer(Configuration.GetConnectionString("DefaultConnection")));
        
            #region Inje��o de Dependencia dos Repositories
            services.AddScoped<IAddressRepository, AddressRepository>();
            services.AddScoped<IEnterpriseRepository, EnterpriseRepository>();
            services.AddScoped<IInvestorRepository, InvestorRepository>();
            services.AddScoped<IUserRepository, UserRepository>();
            #endregion

            #region Inje��o de Dependencia dos Services
            services.AddScoped<IEnterpriseService, EnterpriseService>();
            services.AddScoped<IInvestorService, InvestorService>();
            services.AddScoped<IUserService, UserService>();
            services.AddScoped<IPortfolioService, PortfolioService>();
            #endregion
            services.AddScoped<DbContext, ModelContext>();
            
            services.AddControllers();
            services.AddMvc(c =>
            {
                c.EnableEndpointRouting = false;
            });

            services.AddSwaggerGen(c =>
            {
                c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
                {
                    Version = "v1",
                    Title = "Empresas dotNET API",
                    Description = "APIs proposta por Ioasys",
                    Contact = new Microsoft.OpenApi.Models.OpenApiContact
                    {
                        Name = "Dimitri Carvalho Menezes",
                        Email = "dimitri.menezes@gmail.com"
                    }
                });
            });            
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage(); 
            }                       

            app.UseHttpsRedirection();

            app.UseRouting();

            //app.UseAuthentication();

            app.UseAuthorization();

            app.UseMvc();

            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Empresas dotNET API");
            });

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
            });

            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
        }
    }
}
