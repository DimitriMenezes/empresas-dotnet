﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using EnterpriseApi.Services.Abstract;
using EnterpriseApi.Models;

namespace EnterpriseApi.Controllers
{
    [Route("api/v1/enterprises")]
    [ApiController]
    public class EnterpriseController : Controller
    {
        private readonly IEnterpriseService _enterpriseService;

        public EnterpriseController(IEnterpriseService enterpriseService)
        {
            _enterpriseService = enterpriseService;
        }

        [HttpGet("")]
        public ActionResult GetEnterprises(int? enterprise_types, string name)
        {
            try
            {
                var enterprises = new List<DTOEnterprise>();
                if (enterprise_types == null && name == null)
                {
                    enterprises = _enterpriseService.GetEnterprises();
                }
                else
                {
                    enterprises = _enterpriseService.GetEnterprises(enterprise_types.Value, name);
                }              
              
                var result = new
                {
                    enterprises
                };

                return Ok(result);                
            }
            catch (Exception ex)
            {
                return StatusCode(500,ex);
            }
        }

        [HttpGet("{id}")]
        public ActionResult GetEnterpriseById(int id)
        {
            try
            {
                var enterprise = _enterpriseService.GetEnterpriseById(id);

                if (enterprise == null)
                {
                    object result = new
                    {
                        status = 404,
                        error = "Not Found"
                    };
                    return NotFound(result);
                }
                else
                {
                    var result = new
                    {
                        enterprise,
                        success = true
                    };
                    return Ok(result);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500, ex);
            }          
        }
    }
}