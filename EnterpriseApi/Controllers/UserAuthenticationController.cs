﻿using EnterpriseApi.Models;
using EnterpriseApi.Services.Abstract;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;

namespace EnterpriseApi.Controllers
{
    [Route("api/v1/users/auth")]
    [ApiController]
    public class UserAuthenticationController : ControllerBase
    {
        private readonly IUserService _userService;
        private readonly IInvestorService _investorService;
        private readonly IPortfolioService _portfolioService;

        public UserAuthenticationController(IUserService userService, 
            IInvestorService investorService, 
            IPortfolioService portfolioService)
        {
            _userService = userService;
            _investorService = investorService;
            _portfolioService = portfolioService;
        }
                        
        [HttpPost("sign_in")]
        public ActionResult UserLogin(DTOUserAuthentication user)
        {
            try
            {
                if (_userService.IsAuthenticated(user.email, user.password))
                {
                    var investor = _investorService.GetInvestorByEmail(user.email);

                    var investorPortfolio = _portfolioService.CreateInvestorPortfolio(investor.id);
                    if (investorPortfolio != null)
                    {
                        investor.portfolio = investorPortfolio;
                        investor.portfolio_value = _portfolioService.CalculatePortfolioValue(investor.id);
                    }
                    DTOEnterprise enterprise = null;

                    var result = new
                    {
                        investor,
                        enterprise,
                        success = true
                    };                    

                    return Ok(result);
                }
                else
                {
                    var result = new 
                    {
                        success = false,
                        errors = new List<string>()
                        { "Invalid login credentials. Please try again." }
                    };
                    return Unauthorized(result);
                }
            }
            catch (Exception ex)
            {
                return StatusCode(500,ex);
            }                 
        }       
    }
}
