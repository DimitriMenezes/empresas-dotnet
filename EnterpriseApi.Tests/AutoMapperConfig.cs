﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Entities;
using EnterpriseApi.Models;

namespace EnterpriseApi
{
    public class AutoMapperConfig
    {
        public static IMapper CreateMapper()
        {
            var config = new MapperConfiguration(cfg => {

                cfg.CreateMap<EnterpriseType, DTOEnterpriseType>()
                    .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.enterprise_type_name, opt => opt.MapFrom(src => src.Name));

                cfg.CreateMap<Enterprise, DTOEnterprise>()
                    .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.email_enterprise, opt => opt.MapFrom(src => src.Email))
                    .ForMember(dest => dest.enterprise_name, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dest => dest.description, opt => opt.MapFrom(src => src.Description))
                    .ForMember(dest => dest.facebook, opt => opt.MapFrom(src => src.Facebook))
                    .ForMember(dest => dest.linkedin, opt => opt.MapFrom(src => src.Linkedin))
                    .ForMember(dest => dest.twitter, opt => opt.MapFrom(src => src.Twitter))
                    .ForMember(dest => dest.own_enterprise, opt => opt.MapFrom(src => src.OwnEnterprise))
                    .ForMember(dest => dest.own_shares, opt => opt.MapFrom(src => src.OwnShares))
                    .ForMember(dest => dest.photo, opt => opt.MapFrom(src => src.Photo))
                    .ForMember(dest => dest.phone, opt => opt.MapFrom(src => src.Phone))
                    .ForMember(dest => dest.value, opt => opt.MapFrom(src => src.Value))
                    .ForMember(dest => dest.shares, opt => opt.MapFrom(src => src.Shares))
                    .ForMember(dest => dest.share_price, opt => opt.MapFrom(src => src.SharePrice))
                    .ForMember(dest => dest.enterprise_type, opt => opt.MapFrom(src => src.EnterpriseType))
                    .ForMember(dest => dest.city, opt => opt.MapFrom(src => src.Address.City))
                    .ForMember(dest => dest.country, opt => opt.MapFrom(src => src.Address.Country));

                cfg.CreateMap<Investor, DTOInvestor>()
                    .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.investor_name, opt => opt.MapFrom(src => src.Name))
                    .ForMember(dest => dest.photo, opt => opt.MapFrom(src => src.Photo))
                    .ForMember(dest => dest.first_access, opt => opt.MapFrom(src => src.FirstAccess))
                    .ForMember(dest => dest.balance, opt => opt.MapFrom(src => src.Balance))
                    .ForMember(dest => dest.super_angel, opt => opt.MapFrom(src => src.IsSuperAngel))
                    .ForMember(dest => dest.id, opt => opt.MapFrom(src => src.Id))
                    .ForMember(dest => dest.city, opt => opt.MapFrom(src => src.Address.City))
                    .ForMember(dest => dest.country, opt => opt.MapFrom(src => src.Address.Country));
            });

            return config.CreateMapper();
        }       
    }
}
