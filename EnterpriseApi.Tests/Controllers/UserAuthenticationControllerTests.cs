﻿using AutoMapper;
using Domain.Entities;
using Domain.Repositories.Concrete;
using Domain.Repositories.Concrete.MOCK;
using EnterpriseApi.Controllers;
using EnterpriseApi.Models;
using EnterpriseApi.Services.Abstract;
using EnterpriseApi.Services.Concrete;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Moq;
using System;
using System.Collections.Generic;
using System.Text;

namespace EnterpriseApi.Tests.Controllers
{
    [TestClass]
    public class UserAuthenticationControllerTests
    {
        private IUserService _userService;
        private IInvestorService _investorService;
        private IPortfolioService _portfolioService;
        private IEnterpriseService _enterpriseService;
        private UserAuthenticationController _controller;
        private IMapper _mapper;

        public void Setup()
        {
            _mapper = AutoMapperConfig.CreateMapper();
            _userService = new UserService(new MOCKUserRepository());
            _enterpriseService = new EnterpriseService(new MOCKEnterpriseRepository(), _mapper);
            _portfolioService = new PortfolioService(new MOCKInvestorRepository(), _enterpriseService);
            _investorService = new InvestorService(new MOCKInvestorRepository(), _mapper);

            _controller = new UserAuthenticationController
                (_userService, _investorService, _portfolioService);
        }


        [TestMethod]
        public void UserValidTest()
        {
            Setup();

            dynamic result = _controller.UserLogin
                (new Models.DTOUserAuthentication 
                { 
                    email = "dimitri.menezes@gmail.com", 
                    password = "abc123" 
                });

            Assert.IsTrue(result.StatusCode == 200);
        }

        [TestMethod]
        public void UserWrongPasswordTest()
        {
            Setup();

            dynamic result = _controller.UserLogin
                (new Models.DTOUserAuthentication
                {
                    email = "dimitri.menezes@gmail.com",
                    password = "abc1234"
                });

            Assert.IsTrue(result.StatusCode == 401);
        }

        [TestMethod]
        public void UserWrongEmailTest()
        {
            Setup();

            dynamic result = _controller.UserLogin
                (new Models.DTOUserAuthentication
                {
                    email = "dimitriiii.menezes@gmail.com",
                    password = "abc123"
                });

            Assert.IsTrue(result.StatusCode == 401);
        }
    }
}
