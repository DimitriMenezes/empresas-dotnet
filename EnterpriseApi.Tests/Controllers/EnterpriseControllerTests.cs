﻿using AutoMapper;
using Domain.Repositories.Concrete.MOCK;
using EnterpriseApi.Controllers;
using EnterpriseApi.Services.Abstract;
using EnterpriseApi.Services.Concrete;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Text;

namespace EnterpriseApi.Tests.Controllers
{
    [TestClass]
    public class EnterpriseControllerTests
    {
        private IEnterpriseService _enterpriseService;        
        private IMapper _mapper;
        private EnterpriseController _enterpriseController;

        public void Setup()
        {
            _mapper = AutoMapperConfig.CreateMapper();
            _enterpriseService = new EnterpriseService(new MOCKEnterpriseRepository(), _mapper);
            _enterpriseController = new EnterpriseController(_enterpriseService);
        }

        [TestMethod]
        public void GetAllEnterprisesTest()
        {
            Setup();
            dynamic result = _enterpriseController.GetEnterprises(null,null);
            Assert.IsTrue(result.StatusCode == 200);
        }

        [TestMethod]
        public void ValidNameAndValidTypeIdTest()
        {
            Setup();
            dynamic result =_enterpriseController.GetEnterprises(1,"Fake");
            Assert.IsTrue(result.StatusCode == 200);
        }

        [TestMethod]
        public void ValidNameAndInValidTypeIdTest()
        {
            Setup();
            dynamic result = _enterpriseController.GetEnterprises(0, "Fake");
            Assert.IsTrue(result.StatusCode == 200);
        }

        [TestMethod]
        public void InvalidNameAndValidTypeIdTest()
        {
            Setup();
            dynamic result = _enterpriseController.GetEnterprises(1, "xxxxx");
            Assert.IsTrue(result.StatusCode == 200);
        }

        [TestMethod]
        public void InvalidNameAndInvalidTypeIdTest()
        {
            Setup();
            dynamic result = _enterpriseController.GetEnterprises(0, "xxxxx");
            Assert.IsTrue(result.StatusCode == 200);
        }

        [TestMethod]
        public void ValidEnterpriseIdTest()
        {
            Setup();
            dynamic result = _enterpriseController.GetEnterpriseById(1);
            Assert.IsTrue(result.StatusCode == 200);
        }

        [TestMethod]
        public void InvalidEnterpriseIdTest()
        {
            Setup();
            dynamic result = _enterpriseController.GetEnterpriseById(0);
            Assert.IsTrue(result.StatusCode == 404);
        }
    }
}
